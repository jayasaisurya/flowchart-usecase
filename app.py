from flask import Flask, request, jsonify, send_file
from graphviz import Digraph, Source
import openai, json
from flask_cors import CORS, cross_origin

openai.api_key = 'd5cd9e479fb84ef698dead338e5b8a9b'
openai.api_base = 'https://jayasai01.openai.azure.com/'
openai.api_version = "2023-03-15-preview"
openai.api_type = 'azure'
previous_prompt = ''
app = Flask(__name__)

@app.route('/',methods=['GET'])
@cross_origin()
def hello():
    return jsonify({"status":True,"message":"Backend server is running."})

@app.route('/api/generate-flowchart', methods=['POST'])
def generate_flowchart():
    global previous_prompt
    try:
        data = request.json
        prompt = data.get('prompt', '')
        previous_prompt = prompt
        # Call the flowchart_generation function to generate the flowchart dotcode
        dotcode = flowchart_generation(prompt)

        # Use the Graphviz_Module to generate the image and get the image file path
        image_file = Graphviz_Module(dotcode)

        # Send the image file as a response
        return jsonify({"status":"Dotcode generated successfully."})

    except Exception as e:
        return jsonify({"error": str(e)})

@app.route('/api/get-generated-image', methods=['GET'])
def get_generated_image():
    # Assuming the image file is named 'Output.png' and saved in the same directory
    return send_file('Output.png', mimetype='image/png')

@app.route('/api/regenerate-function',methods=['POST'])
def regenerate_function():
    global previous_prompt
    try:
        dotcode = flowchart_generation(previous_prompt)
        image_file = Graphviz_Module(dotcode)
        return send_file(image_file, mimetype='image/png')
    except Exception as e:
        return jsonify({"error": str(e)})
    
@app.route('/api/get-regenerate-result',methods=['GET'])
def get_regenerate_result():
    return send_file('Output.png', mimetype='image/png')

def flowchart_generation(prompt):
    try:
        response = openai.ChatCompletion.create(
            engine='GPT_3_5',
            temperature=0.7,
            max_tokens=4000,
            top_p=0.95,
            messages = [{"role":"system","content":"I'm a Flowchart Generator tool specialised in generating dotcodes for creating flow charts based on input. I'll get the product/description/requirement about the flowchart from user as input. I'll be following the template given below for presenting the output and I will not mention anything other than the dotcode. Since my task is to specifically generate dotcodes for a proper flowchart, I'll be sticking to some basic rules for creating flowchart, which are mentioned below - \n1)Always format your flow from left to right or top to bottom.\n2)Run your return lines under your flowchart, ensuring they don’t overlap.\n3)Maintain consistent spacing between symbols.\n4)Use the correct symbol for each step (diamond shapes are for decisions, rectangles are for processes, and start/end shapes should be the same, etc.)\n Template :- digraph SoftwareDevelopmentLifeCycle {..."},
                        {"role":"user","content":prompt}])
        return response.choices[-1].message.content
    except Exception as e:
        print(e)

def Graphviz_Module(dotcode):
    dot_code = f"""{dotcode}"""
    graph = Source(dot_code, format='png')
    graph.render(filename='Output')

if __name__ == '__main__':
    CORS(app, support_credentials=True)
    app.run(debug = True,host='0.0.0.0',port=5000)
